

FROM centos:latest

MAINTAINER Missouri National Guard Cyber Team

RUN yum install -y add deltarpm \ 
	nginx \
	epel-release \
	wget \
	unzip \
	php \
	php-mbstring \
	php-pdo \
	php-gd \
	php-ldap
	

VOLUME /etc/ssl/certs/ && \
       /etc/nginx/conf.d && \
       /var/log/nginx/

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

		
RUN cd /var/www/html && \
	wget http://kanboard.net/kanboard-latest.zip && \
	unzip -q kanboard-latest.zip && \
	mkdir /var/www/kanboard && \
	ln -s /opt/kanboard /var/www/kanboard && \
	php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php && \
	php -r "if (hash('SHA384', file_get_contents('composer-setup.php')) === '7228c001f88bee97506740ef0888240bd8a760b046ee16db8f4095c0d8d525f2367663f22a46b48d072c816e7fe19959') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
	php composer-setup.php && \
	php -r "unlink('composer-setup.php');"
	

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]




